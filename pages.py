from locators import UkrStartPageLocators, UkrLoginPageLocators, RegisterPageLocators, PasswordPageLocators, \
    DirectionPageLocators, UkrMainPageLocators, SettingsPageLocators, AddPageLocators, TaskCreationPageLocators, \
    RolePageLocators, SpecPageLocators, AreaPageLocators, SocPageLocators


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class UkrStartPage(BasePage):

    def click_skip(self):
        element = self.driver.find_element(*UkrStartPageLocators.SKIP_CLICK)
        element.click()


class UkrLoginPage(BasePage):

    def click_register(self):
        element = self.driver.find_element(*UkrLoginPageLocators.REGISTER_BUTTON)
        element.click()


class RegisterPage(BasePage):

    def photo(self):
        element = self.drmeiver.find_element(*RegisterPageLocators.PHOTO)
        element.send_keys("test_pict.jpeg")

    def name(self, name):
        element = self.driver.find_element(*RegisterPageLocators.NAME)
        element.send_keys(name)

    def surname(self, surname):
        element = self.driver.find_element(*RegisterPageLocators.SURNAME)
        element.send_keys(surname)

    def mail(self, mail):
        element = self.driver.find_element(*RegisterPageLocators.MAIL)
        element.send_keys(mail)

    def phone(self, phone):
        element = self.driver.find_element(*RegisterPageLocators.PHONE)
        element.send_keys(phone)

    def proceed_btn(self):
        element = self.driver.find_element(*RegisterPageLocators.PROCEED_BUTTON)
        element.click()


class PasswordPage(BasePage):

    def password(self, password):
        element = self.driver.find_element(*PasswordPageLocators.PASS)
        element.send_keys(password)

    def conf(self, password):
        element = self.driver.find_element(*PasswordPageLocators.CONF_PASS)
        element.send_keys(password)

    def spam_btn(self):
        element = self.driver.find_element(*PasswordPageLocators.SPAM)
        element.click()

    def confirm_btn(self):
        element = self.driver.find_element(*PasswordPageLocators.CONFIRM_BUTTON)
        element.click()


class DirectionPage(BasePage):

    def citizen_btn(self):
        element = self.driver.find_element(*DirectionPageLocators.CITIZEN_BUTTON)
        element.click()

    def tradie_btn(self):
        element = self.driver.find_element(*DirectionPageLocators.TRADIE_BUTTON)
        element.click()


class UkrMainPage(BasePage):

    def settings_btn(self):
        element = self.driver.find_element(*UkrMainPageLocators.SETTINGS_BUTTON)
        element.click()

    def tradie_settings_btn(self):
        element = self.driver.find_element(*UkrMainPageLocators.TRADIE_SETTINGS_BUTTON)
        element.click()

    def role_btn(self):
        element = self.driver.find_element(*UkrMainPageLocators.ROLE_BUTTON)
        element.click()


class SettingsPage(BasePage):

    def add_btn(self):
        element = self.driver.find_element(*SettingsPageLocators.ADD_BUTTON)
        element.click()

    def logout_btn(self):
        element = self.driver.find_element(*SettingsPageLocators.LOGOUT_BUTTON)
        element.click()

    def logout1_btn(self):
        element = self.driver.find_element(*SettingsPageLocators.LOGOUT1_BUTTON)
        element.click()


class AddPage(BasePage):

    def title(self, title):
        element = self.driver.find_element(*AddPageLocators.TITLE)
        element.send_keys(title)

    def description(self, description):
        element = self.driver.find_element(*AddPageLocators.DESCRIPTION)
        element.send_keys(description)

    def file(self):
        element = self.driver.find_element(*AddPageLocators.FILE)
        element.send_keys("test_pict.jpeg")

    def next_btn(self):
        element = self.driver.find_element(*AddPageLocators.NEXT_BUTTON)
        element.click()


class TaskCreationPage(BasePage):

    def date_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.DATE_BUTTON)
        element.click()

    def date18_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.DATE18_BUTTON)
        element.click()

    def dead_line_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.DAEDLINE_BUTTON)
        element.click()

    def time_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.TIME_BUTTON)
        element.click()

    def time1(self, time1):
        element = self.driver.find_element(*TaskCreationPageLocators.TIME1)
        element.send_keys(time1)

    def time2(self, time2):
        element = self.driver.find_element(*TaskCreationPageLocators.TIME2)
        element.send_keys(time2)

    def offline_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.OFFLINE_BUTTON)
        element.click()

    def street(self, street):
        element = self.driver.find_element(*TaskCreationPageLocators.STREET)
        element.send_keys(street)

    def home_number(self, home_number):
        element = self.driver.find_element(*TaskCreationPageLocators.HOME_NUMBER)
        element.send_keys(home_number)

    def flat_number(self, flat_number):
        element = self.driver.find_element(*TaskCreationPageLocators.FLAT_NUMBER)
        element.send_keys(flat_number)

    def how_much(self, how_much):
        element = self.driver.find_element(*TaskCreationPageLocators.HOW_MUCH)
        element.send_keys(how_much)

    def publish_btn(self):
        element = self.driver.find_element(*TaskCreationPageLocators.PUBLISH_BUTTON)
        element.click()


class RolePage(BasePage):

    def add_btn(self):
        element = self.driver.find_element(*RolePageLocators.ADD_BUTTON)
        element.click()

    def new_btn(self):
        element = self.driver.find_element(*RolePageLocators.NEW_BUTTON)
        element.click()


class SpecPage(BasePage):

    def cat_btn(self):
        element = self.driver.find_element(*SpecPageLocators.CAT_BUTTON)
        element.click()

    def sub_btn(self):
        element = self.driver.find_element(*SpecPageLocators.SUB_BUTTON)
        element.click()

    def proceed_btn(self):
        element = self.driver.find_element(*SpecPageLocators.PROCEED_BUTTON)
        element.click()


class AreaPage(BasePage):

    def city_btn(self):
        element = self.driver.find_element(*AreaPageLocators.CITY_BUTTON)
        element.click()

    def confirm_btn(self):
        element = self.driver.find_element(*AreaPageLocators.CONFIRM_BUTTON)
        element.click()


class SocPage(BasePage):

    def next_btn(self):
        element = self.driver.find_element(*SocPageLocators.NEXT_BUTTON)
        element.click()
