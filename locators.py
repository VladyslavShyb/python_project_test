from selenium.webdriver.common.by import By


##############################
class UkrStartPageLocators:
    SKIP_CLICK = (By.XPATH, '//*[@id="root"]/div/div/a/div')


class UkrLoginPageLocators:
    REGISTER_BUTTON = (By.XPATH, '//*[@id="root"]/div/form/div[8]/a')


class RegisterPageLocators:
    PHOTO = (By.XPATH, '//*[@id="personal-image"]')
    NAME = (By.XPATH, '//*[@id="root"]/form/div[3]/input')
    SURNAME = (By.XPATH, '//*[@id="root"]/form/div[4]/input')
    MAIL = (By.XPATH, '//*[@id="root"]/form/div[5]/input')
    PHONE = (By.XPATH, '//*[@id="root"]/form/div[6]/input')
    PROCEED_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[7]/button')


class PasswordPageLocators:
    PASS = (By.XPATH, '//*[@id="root"]/form/div[4]/input')
    CONF_PASS = (By.XPATH, '//*[@id="root"]/form/div[5]/input')
    SPAM = (By.XPATH, '//*[@id="root"]/form/div[6]/label/span')
    CONFIRM_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[7]/button')


class DirectionPageLocators:
    CITIZEN_BUTTON = (By.XPATH, '//*[@id="root"]/div/div[3]/div[1]/div/div')
    TRADIE_BUTTON = (By.XPATH, '//*[@id="root"]/div/div[3]/div[2]/div/div')


class UkrMainPageLocators:
    SETTINGS_BUTTON = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div[3]')
    TRADIE_SETTINGS_BUTTON = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div/div[7]')
    ROLE_BUTTON = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div[2]/span')


class SettingsPageLocators:
    ADD_BUTTON = (By.XPATH, '//*[@id="menu-popup"]/div[1]/div[2]/a')
    LOGOUT_BUTTON = (By.XPATH, '//*[@id="menu-popup"]/div[1]/div[2]/div[2]/div/div')
    LOGOUT1_BUTTON = (By.XPATH, '//*[@id="logout-popup"]/div/div[4]')


###########################################
class SpecPageLocators:
    CAT_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[3]/div/div[2]')
    SUB_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[5]/div/div[2]')
    PROCEED_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[6]/button')


class AreaPageLocators:
    CITY_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[3]/div/div[3]')
    CONFIRM_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[6]/button')


class SocPageLocators:
    NEXT_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[6]/button')


class AddPageLocators:
    TITLE = (By.XPATH, '//*[@id="root"]/form/div[2]/input')
    DESCRIPTION = (By.XPATH, '//*[@id="root"]/form/div[5]/textarea')
    FILE = (By.XPATH, '//*[@id="create-task-file"]')
    NEXT_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[7]/button')


class TaskCreationPageLocators:
    DATE_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[2]/input')
    DATE18_BUTTON = (By.XPATH, '/html/body/div[3]/div[2]/div/div[2]/div/span[18]')
    DAEDLINE_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[3]/label/span')
    TIME_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[4]/input')
    TIME1 = (By.XPATH, '/html/body/div[4]/div/div[1]/input')
    TIME2 = (By.XPATH, '/html/body/div[4]/div/div[2]/input')
    OFFLINE_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[5]/label[2]')
    STREET = (By.XPATH, '//*[@id="root"]/form/div[7]/input')
    HOME_NUMBER = (By.XPATH, '//*[@id="root"]/form/div[8]/input')
    FLAT_NUMBER = (By.XPATH, '//*[@id="root"]/form/div[9]/input')
    HOW_MUCH = (By.XPATH, '//*[@id="root"]/form/div[10]/input')
    PUBLISH_BUTTON = (By.XPATH, '//*[@id="root"]/form/div[11]/button')


class RolePageLocators:
    ADD_BUTTON = (By.XPATH, '//*[@id="roles-popup"]/div/div[4]/a')
    NEW_BUTTON = (By.XPATH, '//*[@id="root"]/div/div[3]/div/div')
