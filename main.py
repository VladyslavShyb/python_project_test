import unittest
from selenium import webdriver
import pages
import os
import datetime
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

import random
RANDOM_PHONE = random.randint(1111111111111, 9999999999999)
RANDOM_NUMBER = random.randint(1, 999)
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(50)
        self.driver.get("https://domowoy.ml")
#        self.driver.set_page_load_timeout(100)

    def tests(self):
        try:
            self.driver.delete_all_cookies()
            start_page = pages.UkrStartPage(self.driver)
            sleep(0.8)
            start_page.click_skip()
            sleep(0.8)
            handle = open("output.txt", "a")
            handle.write("Start page is OK" + '\n')
            handle.close()
        except:
            pass
        try:
            login_page = pages.UkrLoginPage(self.driver)
            sleep(0.8)
            login_page.click_register()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Login page is OK' + '\n')
            f.write('Registration button is OK' + '\n')
            f.close()
        except:
            pass
        try:
            register_page = pages.RegisterPage(self.driver)
            sleep(0.8)
            register_page.photo()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter photo on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            register_page.name("Test_name")
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter name on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            register_page.surname("Test_surname")
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter surname on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            register_page.mail(str(RANDOM_PHONE) + "@ukr.net")
            sleep(1.8)
            f = open("output.txt", "a")
            f.write('Enter mail on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            register_page.phone(RANDOM_PHONE)
            sleep(1.8)
            f = open("output.txt", "a")
            f.write('Enter phone on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            register_page.proceed_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Proceed button on registration page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            password_page = pages.PasswordPage(self.driver)
            password_page.password('Qwerty12345')
            f = open("output.txt", "a")
            f.write('Enter password on password page is OK' + '\n')
            f.close()
            sleep(0.8)
            password_page.conf('Qwerty12345')
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Confirm password on password page is OK' + '\n')
            f.close()
            password_page.spam_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Spam button on password page is OK' + '\n')
            f.close()
            password_page.confirm_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Confirm button on password page is OK' + '\n')
            f.write('Direction page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            direction_page = pages.DirectionPage(self.driver)
            sleep(0.8)
            direction_page.citizen_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Citizen profile page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            maine_page = pages.UkrMainPage(self.driver)
            sleep(0.8)
            maine_page.settings_btn()
            sleep(1.4)
            f = open("output.txt", "a")
            f.write('Settings button page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            settings_page = pages.SettingsPage(self.driver)
            sleep(0.8)
            settings_page.add_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Add button is OK' + '\n')
            f.close()
        except:
            pass

        try:
            add_page = pages.AddPage(self.driver)
            sleep(0.8)
            add_page.title('Test_title')
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Title on add page is OK' + '\n')
            f.close()
            dropdown = Select(self.driver.find_element_by_xpath('//*[@id="root"]/form/div[3]/div/select'))
            sleep(1.5)
            dropdown.select_by_visible_text('HOME WORK')
            sleep(1.5)
            f = open("output.txt", "a")
            f.write('Category on add page is OK' + '\n')
            f.close()
            dropdown1 = Select(self.driver.find_element_by_xpath('//*[@id="root"]/form/div[4]/div/select'))
            sleep(1.5)
            dropdown1.select_by_visible_text('Fix table')
            sleep(1.5)
            f = open("output.txt", "a")
            f.write('Subcategory on add page is OK' + '\n')
            f.close()
            add_page.description('Some_description')
            sleep(1.2)
#            add_page.file()
#            sleep(0.8)
#            f = open("output.txt", "a")
#            f.write('Add file on add page is OK' + '\n')
#            f.close()
            add_page.next_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Next button on add page is OK' + '\n')
            f.write('Next button on add page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            task_creation_page = pages.TaskCreationPage(self.driver)
            sleep(0.8)
            task_creation_page.date_btn()
            sleep(0.8)
            task_creation_page.date18_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Date on task creation page is OK' + '\n')
            f.close()
            task_creation_page.dead_line_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Dead line button on task creation page is OK' + '\n')
            f.close()
            task_creation_page.time_btn()
            sleep(0.8)
            task_creation_page.time1(18)
            sleep(0.8)
            task_creation_page.time2(30)
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter dead line button on task creation page is OK' + '\n')
            f.close()
            task_creation_page.offline_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Offline button on task creation page is OK' + '\n')
            f.close()
            dropdown2 = Select(self.driver.find_element_by_xpath('//*[@id="root"]/form/div[6]/div/select'))
            sleep(1.5)
            dropdown2.select_by_visible_text('Lviv')
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Choose city on task creation page is OK' + '\n')
            f.close()
            task_creation_page.street("Test_street")
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter street on task creation page is OK' + '\n')
            f.close()
            task_creation_page.home_number(123)
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter home number on task creation page is OK' + '\n')
            f.close()
            task_creation_page.flat_number(111)
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Enter flat number on task creation page is OK' + '\n')
            f.close()
            task_creation_page.how_much(250)
            sleep(0.8)
            task_creation_page.publish_btn()
            sleep(0.8)
        except:
            pass

        try:
            maine_page.role_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Role button on main page is OK' + '\n')
            f.close()
        except:
            pass

        try:
            sleep(0.8)
            role_page = pages.RolePage(self.driver)
            role_page.add_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Add new role button is OK' + '\n')
            f.close()
            role_page.new_btn()
            sleep(0.8)
        except:
            pass

        try:
            spec_page = pages.SpecPage(self.driver)
            spec_page.cat_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Categories is OK' + '\n')
            f.close()
            spec_page.sub_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Subcategories is OK' + '\n')
            f.close()
            spec_page.proceed_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Proceed button is OK' + '\n')
            f.close()
        except:
            pass

        try:
            area_page = pages.AreaPage(self.driver)
            area_page.city_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Choosing city is OK' + '\n')
            f.close()
            area_page.confirm_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Adding new role (to citizen) is OK' + '\n')
            f.close()
            spec_page.proceed_btn()
            sleep(1.2)
        except:
            pass

        try:
            maine_page.tradie_settings_btn()
            sleep(0.8)
        except:
            pass

        try:
            settings_page.logout_btn()
            sleep(0.8)
            settings_page.logout1_btn()
            sleep(0.8)
            f = open("output.txt", "a")
            f.write('Logout is OK' + '\n')
            f.close()
        except:
            pass


if __name__ == "__main__":
    unittest.main()
